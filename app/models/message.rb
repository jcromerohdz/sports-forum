class Message < ApplicationRecord
  validates :title, presence: true, length: { minimum: 6, maximum: 45 }
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }
  belongs_to :user
  has_many :comments, dependent: :destroy
end
